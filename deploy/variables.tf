variable "prefix" {
  type        = string
  default     = "raad"
  description = "recipe-app-api-devops"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  type        = string
  default     = "shashank@datacultr.com"
  description = "Contact Admin"
}

variable "db_username" {
  description = "Username for RDS psql Instance"
}

variable "db_password" {
  description = "Password for RDS psql Instance"
}

variable "bastion_key_name" {
  default = "shashank-temp-bastian"
}

variable "ecr_image_api" {
  description = "ECR iamge for API"
  default     = "231322554539.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "231322554539.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Django App"
}